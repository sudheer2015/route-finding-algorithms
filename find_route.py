#!/usr/bin/python2

# Assignment URL: https://omega.uta.edu/~gopikrishnav/classes/2018/fall/4308_5360/assmts/assmt1.html

import sys


class Graph(object):
    def __init__(self, line):
        self.start = line.split()[0]
        self.end = line.split()[1]
        self.cost = line.split()[2]


class Construct_graph(object):
    def __init__(self, input_file):
        self.routes = []
        self.input_file = input_file

    def construct_graph(self):
        """@output: src and des in input1 are loaded into self.routes"""

        f = open(self.input_file)
        for line in f:
            line = line.strip()
            # print line
            if "END OF INPUT" in line:
                break

            # Creates graph with src and destination in file
            route = Graph(line)
            self.routes.append(route)

        print "Total no. of routes:", len(self.routes)
        f.close()

    def find_possible_routes(self, key):
        """@output: All possible routes from source to destination
           possible_routes has all connections from src to des, and des to src
        """

        possible_routes = []
        for route in self.routes:
            if key in route.start:
                possible_routes.append(route)
            if key in route.end:
                possible_routes.append(route)

        # Remove visited routes from existing routes
        # Prevents infinite loop
        for route in possible_routes:
            self.routes.remove(route)

        return possible_routes


class Fringe(object):
    def __init__(self, graph):
        self.graph = graph
        self.fringe1 = []

    def add_node(self, node):
        self.fringe1.append(node)
        global expanded_nodes
        # self.fringe1.sort(key=lambda o: o.cost)

        # Uninformed search ==> Uniform cost search ==> Sort fringe by cumulative cost (ascending order)
        # https://algorithmicthoughts.wordpress.com/2012/12/15/artificial-intelligence-uniform-cost-searchucs/
        if arguments[1] == "uninf":
            self.fringe1 = sorted(self.fringe1, key=lambda o: o.cost)

            # No. of nodes expanded
            expanded_nodes += 1

        # Informed search ==> A* algorithm
        # https://algorithmicthoughts.wordpress.com/2013/01/04/artificial-intelligence-a-search-algorithm/
        # Calculate heuristic function ==> f(n) = g(n) + h(n)
        # Where, f(n) is resultant cost, g(n) is cost to goal node, h(n) is heuristic to goal node
        elif arguments[1] == "inf":
            for temp in self.fringe1:
                temp.cost = int(temp.cost) + heuristics[temp.current]

            # Sort fringe based on heuristic cost to goal node
            self.fringe1 = sorted(self.fringe1, key=lambda o: o.cost)

            # Remove heuristic cost after sorting
            # Because every time the cost has to be calculated. Using this way avoids adding same value multiple times
            for temp in self.fringe1:
                temp.cost = int(temp.cost) - heuristics[temp.current]

            # No. of nodes expanded
            expanded_nodes += 1

    def remove_node(self):
        # Remove first node in sorted fringe (BFS search)
        return self.fringe1.pop(0)

    def fringe_size(self):
        return len(self.fringe1)


class Tree_node(object):
    def __init__(self, current, parent, child, depth, cost):
        self.current = current
        self.parent = parent
        self.child = child
        self.depth = depth
        self.cost = cost

    def check_current(self, current):
        return self.current == current


def load_heuristics():
    """@output: Src and des in input1 are loaded into self.routes"""

    global heuristics
    heuristics = {}

    f = open(heuristics_file)
    for line in f:
        line = line.strip()
        # print line
        if "END OF INPUT" in line:
            break

        # Create a dictionary of heuristics values in file
        # c ==> City, h ==> heuristic
        c, h = line.split()
        heuristics[c] = int(h)

    print "Heuristics count:", len(heuristics)
    f.close()


class Find_route(object):
    def __init__(self, src, des, graph):
        self.src = src
        self.des = des
        self.root = None
        self.graph = graph

    def construct_tree(self):
        fringe = Fringe(self.graph)
        self.root = Tree_node(self.src, None, [], 0, 0)
        fringe.add_node(self.root)
        flag = False

        while fringe.fringe_size() != 0:
            current_node = fringe.remove_node()

            if current_node.check_current(self.des):
                print "\n************************************"
                print "Total expanded nodes: ", expanded_nodes
                print "************************************\n"
                print "distance: %d km" % current_node.cost
                print "route:"

                while current_node.parent is not None:
                    current_node.parent.child.append(current_node)
                    current_node = current_node.parent

                flag = True
                break

            possible_routes = self.graph.find_possible_routes(current_node.current)

            new_node = None
            # If path contains start_node or end_node, add it to fringe
            for route in possible_routes:
                # print current_node, route.start
                # Check if current_node is start of route
                if current_node.check_current(route.start):
                    new_node = Tree_node(route.end, current_node, [], route.cost, current_node.cost + int(route.cost))

                # Check if current_node is end of route
                elif current_node.check_current(route.end):
                    new_node = Tree_node(route.start, current_node, [], route.cost, current_node.cost + int(route.cost))

                # Whether if node is start or end of route, add the node to fringe
                fringe.add_node(new_node)

        if not flag:
            print "distance: Infinity"
            print "route: \n none"
        else:
            current_node = self.root
            while current_node.child:
                print "%s to %s, %s km" % (current_node.current, current_node.child[0].current, current_node.child[0].depth)
                current_node = current_node.child[0]


if __name__ == "__main__":
    arguments = sys.argv
    # No. of expanded nodes
    expanded_nodes = 0

    if arguments[1] == "uninf":
        input_file, src, des = arguments[2], arguments[3], arguments[4]
        print "================================"
        print "Performing uninformed search"
        print "================================\n"

        # Build graph from input_file.txt
        obj = Construct_graph(input_file)
        obj.construct_graph()

        # Find route from source to destination
        obj1 = Find_route(src, des, obj)
        obj1.construct_tree()

    elif arguments[1] == "inf":
        input_file, src, des, heuristics_file = arguments[2], arguments[3], arguments[4], arguments[5]
        print "================================"
        print "Performing informed search"
        print "================================\n"

        # Load heuristics file
        load_heuristics()

        # Build graph from input_file.txt
        obj = Construct_graph(input_file)
        obj.construct_graph()

        # Find route from source to destination
        obj1 = Find_route(src, des, obj)
        obj1.construct_tree()











