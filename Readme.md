# Route finding algoritm


> **Caution:** To be used for education only. This project is done for the CSE 5360 Artificial Intelligence 1 course taught at UTA. If you use this project's code for the same course, you might be liable for copyright problems.


## Requirements

* Python 2.6

## Demo

<p align="center"><img src="https://gitlab.com/sudheer2015/route-finding-algorithms/raw/master/images/AI_1.gif" alt="Video demo"/></p>

## Description

* **Main method:** 	The main method parses command line arguments passed by the user. The user can specify the following command line parameters, type of search, graph file, source and destination. If user selects informed search, he needs to provide an additional heuristic file as argument. Graph file and heuristic file should be in same directory of **'find_route.py'**. 

* **Graph node:** Create graph nodes (Graph) of available paths in graph_file.txt.
* **Construct_graph:** Build a route list of graph nodes.
* **Find_possible_routes:** Given a key (Start of route), the function returns list of all possible routes to destination. Possible routes are deleted from available routes list.
* **Fringe:** This class provides various functions as follows. The main function of this class is to create and maintain a fringe queue.
* **Add_node:** Add a node to fringe queue
* **Remove_node:** Remove a node from fringe queue.
* **Fringe_size:** Returns current fringe size
* **Tree_node:** This class creates tree_node and has following properties. It has parent, child, depth and cost from root node to current node. The current property holds name of current city or state. 
* **Load_heuristics:** The main function of this class is to load heuristics data from file into a dictionary. This information is used by A* algorithm (informed search) to find path from source to destination.
* **Find_route:** This class is crux to this program. This class creates a fringe using 'Fringe' node structure, creates a tree node using 'Tree_node' structure and finds optimal path from source to destination. It provides following functions.
* **Construct_tree:** Build a tree with root, current and children nodes from source to destination. 

Since python is interpreter-based language, it doesn't require compiling code prior to running it. 

The uninformed search can be executed as follows. 

> python <name_of_program> <type_of_search> <graph_file.txt> <source> <destination> 

> python find_route.py uninf input1.txt Bremen Frankfurt 

The program prints route to destination as below

*Output:*

```bash
================================
Performing uninformed search
================================
Total no. of routes: 28
************************************
Total expanded nodes:  18
************************************
distance: 455 km
route:
Bremen to Dortmund, 234 km
Dortmund to Frankfurt, 221 km
```

The informed search can be executed as follows.

> python <name_of_program> <type_of_search> <graph_file.txt> <source> <destination> 

> python find_route.py inf input1.txt Munich Kassel h_kassel.txt

The program prints route to destination as below.

```bash
================================
Performing informed search
================================
Heuristics count: 21
Total no. of routes: 28

************************************
Total expanded nodes:  14
************************************
distance: 578 km
route:
Munich to Nuremberg, 171 km
Nuremberg to Frankfurt, 222 km
Frankfurt to Kassel, 185 km
```

## Exit condition

The function terminates in two situations, when it finds a route to destination and when it cannot find a route to destination. If it finds a route, it traveses from child node to root node and prints route and cost to reach destination. If prints as follows when it cannot find route to destination.

```bash
================================
Performing uninformed search
================================
Total no. of routes: 28
distance: Infinity
route: 
 none
```

Libraries required: All required libraries are available on omega server. The program can be executed as follows.

## Run instructions

**Uninformed search (Uniform cost search):**

> python find_route.py uninf input1.txt Bremen Frankfurt 

**Informed search (A* algorithm):**

> python find_route.py inf input1.txt Munich Kassel h_kassel.txt

## References

1.	https://www.youtube.com/watch?v=V1i7EcynYKk
2.	https://algorithmicthoughts.wordpress.com/2012/12/15/artificial-intelligence-uniform-cost-searchucs/
3.	https://algorithmicthoughts.wordpress.com/2013/01/04/artificial-intelligence-a-search-algorithm/
4.	https://stackoverflow.com/questions/43354715/uniform-cost-search-in-python
5.	https://github.com/marcoscastro/ucs
6.	https://piotechno.wordpress.com/2014/01/23/uniform-cost-and-a-search-algorithm/
7.	https://stackoverflow.com/questions/14091387/creating-a-dictionary-from-a-csv-file
8.	https://stackoverflow.com/questions/4803999/how-to-convert-a-file-into-a-dictionary
9.	https://brilliant.org/wiki/a-star-search/
10.	https://stackoverflow.com/questions/2358045/how-can-i-implement-a-tree-in-python-are-there-any-built-in-data-structures-in
11.	http://knuth.luther.edu/~leekent/CS2Plus/chap6/chap6.html
12.	https://pythonspot.com/python-tree/
13.	https://interactivepython.org/runestone/static/pythonds/Trees/SearchTreeImplementation.html